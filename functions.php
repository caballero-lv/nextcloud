<?php

// JS for window refresh after upload and so

function nc_importer_admin()
{
  wp_enqueue_script('nc-importer', plugin_dir_url(__FILE__) .  '/functions.js', array('jquery'), '1.0.0', true);
}
add_action('admin_enqueue_scripts', 'nc_importer_admin');



// Check if we have necessary creds, throw warning otherwise 

$options = get_option('nc_plugin_options');

if (isset($options['nc_base_url']) && isset($options['nc_user']) && $options['nc_pass']) {

  add_action('pre-plupload-upload-ui', 'nc_pre_plupload_upload_ui');

  if (!session_id()) {
    session_start();
  }

  $_SESSION['nc_base_url'] = $options['nc_base_url'];
  $_SESSION['nc_user'] = $options['nc_user'];
  $_SESSION['nc_pass'] = $options['nc_pass'];


} else {

  add_action( 'admin_notices', 'nc_admin_notice__error' );


}






function nc_admin_notice__error() {
  $class = 'notice notice-error';
  $message = '<strong>NextCloud importer:</strong> Please enter all necessary credentials for <a href="' . admin_url( 'options-general.php?page=nc-importer') . '">NextCloud importer</a>!';

  printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ),  $message ); 
}


/**
 * Implements 'pre-plupload-upload-ui'.
 * Add External Media plugin buttons to Upload File tab.
 */
function nc_pre_plupload_upload_ui()
{
?>
  <button class="button button-hero" onclick="ncWindow('<?php echo plugin_dir_url(__FILE__); ?>browser/client.php?list=/remote.php/dav/files/<?php echo $_SESSION['nc_user']; ?>')" type="button">Import from NextCloud</button>
<?php
}


add_action('rest_api_init', function () {


  register_rest_route('nc-import/v1', '/file', array(
    'methods' => 'POST',
    'callback' => 'nc_do_import',
    'permission_callback' => '__return_true',
    'args' => array(
      'file' => array(),
      'author' => array(),
      'description' => array(),
    ),
  ));
});


function nc_do_import($data)
{

  $options = get_option('nc_plugin_options');
  $author = $data['author'];
  $description = $data['description'];
  // pre($data);
  // die;

  $url = $options['nc_base_url'] . $data['file'];
  $curl = curl_init($url);
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_USERPWD, $options['nc_user'] . ":" . $options['nc_pass']);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($curl, CURLOPT_HEADER, TRUE);

  $response = curl_exec($curl);

  $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
  $header = substr($response, 0, $header_size);
  $file = substr($response, $header_size);


  $headers = array();

  foreach (explode("\r\n", $header) as $i => $line)
    if ($i === 0)
      $headers['http_code'] = $line;
    else {

      list($key, $value) = array_pad(explode(': ', $line), 2, null);

      $headers[$key] = $value;
    }


  $filename = basename($url);
  $parent_post_id = 0;

  $upload_file = wp_upload_bits($filename, null, $file);
  if (!$upload_file['error']) {
    $wp_filetype = wp_check_filetype($filename, null);
    $attachment = array(
      'post_mime_type' => $wp_filetype['type'],
      'post_parent' => $parent_post_id,
      'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
      'post_excerpt' => sanitize_text_field( $author ),
      'post_content' => sanitize_text_field( $description ),
      'post_status' => 'inherit'
    );
    $attachment_id = wp_insert_attachment($attachment, $upload_file['file'], $parent_post_id);
    if (!is_wp_error($attachment_id)) {
      require_once(ABSPATH . "wp-admin" . '/includes/image.php');
      $attachment_data = wp_generate_attachment_metadata($attachment_id, $upload_file['file']);
      wp_update_attachment_metadata($attachment_id,  $attachment_data);
    }
  } else {
    echo "error";
  }


  // if folder id privided and wp media folder plugin aivailable, move to folder
  if (file_exists(WP_PLUGIN_DIR . '/wp-media-folder/helper.php') && isset($data['wpmf_folder'])) {

    include(WP_PLUGIN_DIR . '/wp-media-folder/helper.php');

    \Joomunited\WPMediaFolder\Helper::moveFile($attachment_id, $data['wpmf_folder']);
  }




  echo $attachment_id;
  exit;
}




// Settings page

function nc_add_settings_page()
{
  add_options_page('NextCloud importer settings', 'NextCloud importer', 'manage_options', 'nc-importer', 'nc_render_plugin_settings_page');
}
add_action('admin_menu', 'nc_add_settings_page');


function nc_render_plugin_settings_page()
{
?>
  <h2>NextCloud importer settings</h2>
  <form action="options.php" method="post">
    <?php
    settings_fields('nc_plugin_options');
    do_settings_sections('nc_plugin'); ?>
    <input name="submit" class="button button-primary" type="submit" value="<?php esc_attr_e('Save'); ?>" />
  </form>
<?php
}

function nc_register_settings()
{
  register_setting('nc_plugin_options', 'nc_plugin_options', 'nc_plugin_options_validate');
  add_settings_section('api_settings', '', 'nc_plugin_section_text', 'nc_plugin');

  add_settings_field('nc_plugin_setting_base_url', 'NextCloud base URL', 'nc_plugin_setting_base_url', 'nc_plugin', 'api_settings');
  add_settings_field('nc_plugin_setting_nc_user', 'Username', 'nc_plugin_setting_nc_user', 'nc_plugin', 'api_settings');
  add_settings_field('nc_plugin_setting_nc_pass', 'Password', 'nc_plugin_setting_nc_pass', 'nc_plugin', 'api_settings');
}
add_action('admin_init', 'nc_register_settings');


function nc_plugin_section_text()
{
}

function nc_plugin_setting_base_url()
{
  $options = get_option('nc_plugin_options');
  echo "<input id='nc_plugin_setting_base_url' name='nc_plugin_options[nc_base_url]' type='text' value='" . esc_attr(isset($options['nc_base_url']) ? $options['nc_base_url'] : '')  . "' />";
}

function nc_plugin_setting_nc_user()
{
  $options = get_option('nc_plugin_options');
  echo "<input id='nc_plugin_setting_nc_user' name='nc_plugin_options[nc_user]' type='text' value='" . esc_attr(isset($options['nc_user']) ? $options['nc_user'] : '')  . "' />";
}

function nc_plugin_setting_nc_pass()
{
  $options = get_option('nc_plugin_options');
  echo "<input id='nc_plugin_setting_nc_pass' name='nc_plugin_options[nc_pass]' type='password' value='" . esc_attr(isset($options['nc_pass']) ? $options['nc_pass'] : '') . "' />";
}

function nc_plugin_options_validate($input)
{


if(!function_exists('curl_init')) {
  die('cURL not available!');
}

$options = get_option('nc_plugin_options');


$curl = curl_init();
// or use https://httpbin.org/ for testing purposes
$url = $input['nc_base_url'];
$curl = curl_init($url);
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_USERPWD, $input['nc_user'] . ":" . $input['nc_pass']);
curl_setopt($curl, CURLOPT_FAILONERROR, true);
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);


$output = curl_exec($curl);
if ($output === FALSE) {

  add_settings_error(
    'nc_plugin_options', // whatever you registered in `register_setting
    'curl_error', // doesn't really mater
    '<strong>NextCloud importer:</strong> Connection failed. Please check your credentials and try again!',
    'error', // error or notice works to make things pretty
  );

}
$input['nc_base_url'] = rtrim($input['nc_base_url'], "/");


  return $input;

}

// 