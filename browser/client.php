<?php

if (!session_id()) {
  session_start();
}

use Sabre\DAV\Client;

include 'vendor/autoload.php';




$settings = array(
    'baseUri' => $_SESSION['nc_base_url'],
    'userName' =>  $_SESSION['nc_user'],
    'password' => $_SESSION['nc_pass'],
);

$client = new Client($settings);




if (isset($_GET["list"])):


  $path = ($_GET["list"]) ? $_SESSION['nc_base_url'] .  implode('/', array_map('rawurlencode', explode('/', $_GET["list"]))) : $_SESSION['nc_base_url'] . 'remote.php/dav/files/' . $_SESSION['nc_user'] . '/';

  $dav_fields = array(
    '{DAV:}getcontenttype',
    '{DAV:}resourcetype',
    '{http://owncloud.org/ns}author',
    '{http://owncloud.org/ns}description',
    '{http://nextcloud.org/ns}has-preview',
    '{http://owncloud.org/ns}fileid'
  );

try {
  $folder_content = $client->propFind($path, $dav_fields, 1);

} catch (Exception $e) {
  
  include "views/error.php";
  exit;
  
}
  

  include "views/list.php";

endif;


  // echo "<pre>";
// print_r($folder_content);

