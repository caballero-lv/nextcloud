$(function () {
  $('.file-card').on('click', function () {
    $(this).toggleClass('selected');

    if ($('.selected').length > 0) {
      $('.do-import').prop('disabled', false)
    } else {
      $('.do-import').prop('disabled', true)
    }
  });

  $('.do-import').on('click', function () {

    $('.do-import').prop('disabled', true);
    var item_ids = [];


    $('.selected').each(function () {

      var url = $(this).data('url');
      var description = $(this).data('description');
      var author = $(this).data('author');
      var item = $(this);

      var wpmf_folder = 0;

      if(localStorage.getItem('wpmf_folder')) {
        wpmf_folder = localStorage.getItem('wpmf_folder');
      }


      $(item).removeClass('selected');
      $(item).addClass('loading');

      $.post("/wp-json/nc-import/v1/file/", { file: url, wpmf_folder: wpmf_folder, author: author, description: description }).always(function (data) {
        $(item).removeClass('loading');
        $(item).addClass('done');

        item_ids.push(data);


        if ($('.loading').length == 0) {
          window.opener.ncMediaReload(item_ids);
          window.close();
        }

      });




    });


  });

  $('.folder-card a').on('click', function(){
    $(this).closest('.folder-card').addClass('loading');
  });
});