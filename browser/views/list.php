<!DOCTYPE html>
<html>

<head>
  <title>NextCloud browser</title>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
  <link rel="stylesheet" href="../browser/assets/styles/style.css" />
</head>

<body>
  <div class="container">
    <?php


    foreach ($folder_content as $item_path => $item_content) :

      if (isset($item_content["{DAV:}getcontenttype"])) {

        $files[$item_path] = $item_content;
      } else {

        $folders[$item_path] = $item_content;
      }

    endforeach;

    ?>


    <div class="folders mt-4 mb-4">
      <div class="row">
        <?php

        $i = 0;

        foreach ($folders as $item_path => $item_content) :

          $path_array = explode('/', $item_path);
          end($path_array);
          $folder_name = urldecode(prev($path_array));

          $icon = "bi-folder-fill";
          $label = $folder_name;

          if ($i == 0) {
            if ($folder_name == $_SESSION['nc_user']) {
              // skip this item if it seems to be root
              $i++;
              continue;
            } else {
              $icon = "bi-arrow-up-square-fill";
              $label = "One level up";
              $item_path = implode('/', array_splice(explode('/', $item_path), 0, -2));
            }
          }


        ?>
          <div class="col-12 col-sm-6 col-md-4 col-lg-3">
            <div class="card mb-3 folder-card">
              <div class="card-body">
                <i class="bi <?php echo $icon; ?>">

                <div class="spinner loadingio-spinner-spinner-xjct47135ml"><div class="ldio-4p4phwxwvdc">
<div></div><div></div><div></div><div></div><div></div><div></div><div></div>
</div></div>
                
              </i> <a href="?list=<?php echo $item_path; ?>"><?php echo $label; ?></a>
              </div>
            </div>
          </div>
        <?php
          $i++;

        endforeach;

        ?>
      </div>
    </div>

    <div class="files mb-4">
      <div class="row">

<?php
        if (is_array($files)) :

       foreach ($files as $item_path => $item_content) :

            $path_array = explode('/', $item_path);
            $file_name = urldecode(end($path_array));

            // slip dotfiles
            if ($file_name[0] == '.') {
              continue;
            }

?>
            <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-4">
              <div class="card file-card" data-url="<?php echo $item_path; ?>" data-author="<?php echo $item_content["{http://owncloud.org/ns}author"]; ?>"  data-description="<?php echo $item_content["{http://owncloud.org/ns}description"]; ?>">
                <div class="card-img-top">

                  <i class="bi bi-file-earmark"></i>

                  <?php
                  if ($item_content["{http://nextcloud.org/ns}has-preview"]) :
                  ?>
                    <div class="file-preview" style="background-image: url(views/preview.php?id=<?php echo $item_content["{http://owncloud.org/ns}fileid"]; ?>);"></div>
                  <?php
                  endif;
                  ?>



                </div>
                <div class="card-body">
                  <?php echo $file_name; ?>
                </div>
                <div class="spinner">
                  <div class="loadingio-spinner-spinner-1q3rcs6t4gg">
                    <div class="ldio-bqauav5xs6">
                      <div></div>
                      <div></div>
                      <div></div>
                      <div></div>
                      <div></div>
                      <div></div>
                      <div></div>
                      <div></div>
                      <div></div>
                      <div></div>
                      <div></div>
                    </div>
                  </div>
                </div>

              </div>
            </div>

<?php

endforeach;

        else :

          ?>
          <div class="container">

            <div class="alert alert-secondary" role="alert">
              This folder is empty
            </div>
          </div>
        <?php

endif;
?>
</div>

</div>


      </div>
    </div>

<div class="bottom-bar">
    <div class="container">
      <button type="button" class="btn btn-primary do-import" disabled>Import selected</button>
    </div>
  </div>

  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  <script src="../browser/assets/scripts/script.js"></script>

</body>

</html>
