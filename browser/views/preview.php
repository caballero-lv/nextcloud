<?php
// getting file previews via curl, dunno why but seems to work somehow

if (!session_id()) {
   session_start();
 }



$prev_id = $_GET['id'];

$url = $_SESSION['nc_base_url'] . "/index.php/core/preview?fileId=".$prev_id."&x=300&y=300&a=true";


$curl = curl_init($url);
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_USERPWD, $_SESSION['nc_user'] . ":" . $_SESSION['nc_pass']);  
curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($curl, CURLOPT_HEADER, TRUE);

$response = curl_exec($curl);

$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
$header = substr($response, 0, $header_size);
$body = substr($response, $header_size);


$headers = array();

foreach (explode("\r\n", $header) as $i => $line)
   if ($i === 0)
      $headers['http_code'] = $line;
   else {

      list($key, $value) = array_pad(explode(': ', $line), 2, null);

      $headers[$key] = $value;
   }

header('Content-Type:'.$headers['Content-Type']);
echo $body;

?>
