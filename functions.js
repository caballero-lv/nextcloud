function ncMediaReload(item_ids) {

  if (!jQuery('body').hasClass('post-type-attachment')) {
    jQuery('#menu-item-browse').trigger('click');
  }

  if (wp.media) {

    wp.media.frame.content.get().collection._requery(true);
    if (!jQuery('body').hasClass('post-type-attachment')) {

      setTimeout(function () {
        item_ids.forEach(function (id) {
          jQuery('[data-id="' + id + '"]').trigger('click');
        })
      }, 1000);
    }

  }




};

function ncWindow(url) {

  if(typeof wpmfFoldersModule != 'undefined' && typeof wpmfFoldersModule.last_selected_folder != 'undefined') {
    localStorage.setItem('wpmf_folder', wpmfFoldersModule.last_selected_folder);
  }

  window.open(url,'targetWindow',
                                   `toolbar=no,
                                    location=no,
                                    status=no,
                                    menubar=no,
                                    scrollbars=yes,
                                    resizable=yes,
                                    width=1200,
                                    height=800`);

 return false;
}